package com.at.learning.batch.writer;

import com.at.learning.batch.domain.Person;
import com.at.learning.batch.entity.PersonEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cassandra.core.ConsistencyLevel;
import org.springframework.cassandra.core.WriteOptions;
import org.springframework.data.cassandra.core.CassandraOperations;

import java.util.List;
import java.util.UUID;

/**
 * Custom Item Writer class to write to Cassandra table.
 */
public class CassandraBatchItemWriter implements ItemWriter<Person> {

    /**
     * Logger
     */
    protected static final Logger logger = LoggerFactory.getLogger(CassandraBatchItemWriter.class);

    /**
     * CassandraOperations
     */
    @Autowired
    private CassandraOperations cassandraOperations;

    /**
     * Write method
     * @param personEntitiesList
     * @throws Exception on error.
     */
    @Override
    public void write(List<? extends Person> personEntitiesList) throws Exception {
        logger.info("Write operations will write " + personEntitiesList.size() + " rows.");
        if (!personEntitiesList.isEmpty()) {
            WriteOptions writeOptions = new WriteOptions();
            writeOptions.setConsistencyLevel(ConsistencyLevel.QUOROM);
            logger.info("Writing to table");
            for (Person row : personEntitiesList) {
                PersonEntity personEntity = new PersonEntity();
                personEntity.setId(UUID.randomUUID());
                personEntity.setFirst_name(row.getFirstName());
                personEntity.setLast_name(row.getLastName());
                personEntity.setAge(row.getAge());
                cassandraOperations.insert(personEntity, writeOptions);
            }
        }

    }

}