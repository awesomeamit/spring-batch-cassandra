package com.at.learning.batch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Person domain class.
 */
@Getter
@Setter
@ToString
public class Person implements Serializable {

    /**
     * First Name
     */
    private String firstName;

    /**
     * Last Name
     */
    private String lastName;

    /**
     * Age
     */
    private int age;
}