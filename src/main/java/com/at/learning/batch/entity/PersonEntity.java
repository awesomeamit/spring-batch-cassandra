package com.at.learning.batch.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.io.Serializable;
import java.util.UUID;


/**
 * Person Entity Class
 */
@Table(value = "person")
@Getter
@Setter
@ToString
public class PersonEntity implements Serializable {

    /**
     * Primary Key column id of type UUID
     */
    @PrimaryKey(value = "id")
    private UUID id;

    /**
     * First name
     */
    @Column(value = "first_name")
    private String first_name;

    /**
     * Last Name
     */
    @Column(value = "last_name")
    private String last_name;

    /**
     * Age
     */
    @Column(value = "age")
    private int age;
}
