package com.at.learning.batch;

import com.at.learning.batch.configuration.BatchConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


/**
 * Batch Application main class.
 */
@SpringBootApplication
@Import(BatchConfiguration.class)
public class CassandraBatchApplication implements CommandLineRunner {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(CassandraBatchApplication.class, args);
    }

    @Override
    public void run(final String... strings) throws Exception {
    }

}
