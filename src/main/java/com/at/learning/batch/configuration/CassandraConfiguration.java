package com.at.learning.batch.configuration;

import com.at.learning.batch.entity.PersonEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.convert.CassandraConverter;
import org.springframework.data.cassandra.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

/**
 * This is the configuration class for Cassandra. This class injects beans for connecting to Cassandra Cluster and
 * create session.
 */
@Configuration
@PropertySource(value={"classpath:cassandra.properties"})
@EnableCassandraRepositories(basePackages = { "RepositoryPackage.class" })
public class CassandraConfiguration extends AbstractCassandraConfiguration {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CassandraConfiguration.class);

    /**
     * Environment
     */
    @Autowired
    private Environment environment;

    /**
     * Method to get Cassandra Keyspace
     * @return Keyspace name
     */
    @Override
    protected String getKeyspaceName() {
        return environment.getProperty("cassandra.spring.batch.keyspace");
    }

    /**
     * Method to get Cassandra cluster contact points
     * @return Contact points
     */
    @Override
    protected String getContactPoints() {
        return environment.getProperty("cassandra.contactpoints");
    }

    /**
     * Method to get Cassandra CQL port
     * @return CQL port
     */
    @Override
    protected int getPort() {
        return Integer.parseInt(environment.getProperty("cassandra.port"));
    }


    /**
     * Cassandra cluster factory bean
     * @return CassandraClusterFactoryBean
     */
    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean clusterFactoryBean = new CassandraClusterFactoryBean();
        clusterFactoryBean.setContactPoints(environment.getProperty("cassandra.contactpoints"));
        return clusterFactoryBean;

    }


    /**
     * Mapping context
     * @return CassandraMappingContext
     */
    @Bean
    public CassandraMappingContext mappingContext() {
        return new BasicCassandraMappingContext();
    }

    /**
     * Converter
     * @return CassandraConverter
     */
    @Bean
    public CassandraConverter converter() {
        return new MappingCassandraConverter(mappingContext());
    }

    /**
     * Session Factory Bean to create session and connect to a keyspace
     * @return CassandraSessionFactoryBean
     * @throws Exception when connection fails
     */
    @Bean
    public CassandraSessionFactoryBean sessionFactoryBean() throws Exception {

        CassandraSessionFactoryBean session = new CassandraSessionFactoryBean();
        session.setCluster(cluster().getObject());
        session.setKeyspaceName(getKeyspaceName());
        session.setConverter(converter());

        return session;
    }

    /**
     * EntityBeanPackage method
     * @return Array of Entity
     */
    @Override
    public String[] getEntityBasePackages() {
        return new String[] {PersonEntity.class.getPackage().getName()};
    }

    /**
     * CassandraTemplate bean
     * @return CassandraTemplate object
     * @throws Exception when unable to create template.
     */
    @Bean(name = "cassandraTemplate")
    public CassandraTemplate profileCassandraTemplate() throws Exception {
        CassandraTemplate cassandraTemplate = new CassandraTemplate(sessionFactoryBean().getObject(), converter());
        LOG.debug("Calling logCassandraTemplateCreation to log cluster details.");
        return cassandraTemplate;
    }
}
