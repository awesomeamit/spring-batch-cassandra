package com.at.learning.batch.configuration;

import com.at.learning.batch.mapper.PersonRowMapper;
import com.at.learning.batch.domain.Person;
import com.at.learning.batch.writer.CassandraBatchItemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

/**
 * A demo batch configuration for a simple batch job that reads data from a file and writes to a Cassandra table. This
 * demo uses Spring boot batch.
 */
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
@Import(CassandraConfiguration.class)
@ComponentScan(basePackages = "com.at.learning")
public class BatchConfiguration {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CassandraConfiguration.class);

    /**
     * Job Builder Factory
     */
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    /**
     * Step Builder Factory
     */
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    /**
     * Item Reader bean using FlatFileItemReader. Reader will read from a CSV file
      * @return FlatFileItemReader object
     */
    @Bean
    public FlatFileItemReader<Person> reader() {
        LOG.info("Inside Reader");
        FlatFileItemReader<Person> reader = new FlatFileItemReader<Person>();
        reader.setResource(new ClassPathResource("sample-data.csv"));

        //Using DefaultLineMapper as it uses comma as delimiter by default
        DefaultLineMapper<Person> lineMapper = new DefaultLineMapper<Person>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"firstName", "lastName", "age"});
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new PersonRowMapper());
        reader.setLineMapper(lineMapper);
        return reader;
    }

    /**
     * Item writer bean. Calls a customer item writer to write to Cassandra table.
     * @return ItemWriter object
     */
    @Bean
    public ItemWriter writer() {
        CassandraBatchItemWriter writer = new CassandraBatchItemWriter();
        return writer;
    }

    /**
     * Job Bean. This job illustrate use of flow control in steps.
     * @param jobs
     * @param s1
     * @return Job
     */
    @Bean
    public Job job1(final JobBuilderFactory jobs, final Step s1) {
        return jobBuilderFactory.get("job1")
                .incrementer(new RunIdIncrementer())
                .flow(s1)
                .end()
                .build();
    }

    /**
     * Read from file write to table step bean
     * @param stepBuilderFactory
     * @param reader
     * @param writer
     * @return Step
     */
    @Bean
    public Step step1(final StepBuilderFactory stepBuilderFactory, final ItemReader<Person> reader,
                      final ItemWriter writer) {
        return stepBuilderFactory.get("step1")
                .<Person, Person> chunk(10)
                .reader(reader)
                .writer(writer)
                .build();
    }
}